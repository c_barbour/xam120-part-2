using Foundation;
using UIKit;
using Xamarin.Forms;

using CrossPhoneword.iOS;

[assembly: Dependency(typeof(PhoneDialer))]
namespace CrossPhoneword.iOS
{
    public class PhoneDialer : IDialer
    {
        public bool Dial(string number)
        {
            return UIApplication.SharedApplication.OpenUrl(
				new NSUrl("tel:" + number));
        }
    }
}
