﻿using System;
using Xamarin.Forms;

namespace CrossPhoneword
{
	public class MainPage: ContentPage
	{
		Entry m_enterNumber;
		Button m_translateButton;
		Button m_callButton;

		public MainPage()
		{
			this.Padding = new Thickness(20, Device.OnPlatform(40, 20, 20), 20, 20);

			StackLayout panel = new StackLayout 
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Vertical,
				Spacing = 15,
			};

			AddUIElementsAsChildren(panel);
			AddListeners();

			this.Content = panel;
		}

		~MainPage()
		{
			m_callButton.Clicked -= callButton_Clicked;
			m_translateButton.Clicked -= translateButton_Clicked;
		}

		private void AddUIElementsAsChildren(StackLayout layout)
		{
			
			layout.Children.Add(new Label {
				Text = "Enter a phoneword:",
			});

			layout.Children.Add(m_enterNumber = new Entry {
				Text = "1-855-XAMARIN",
			});

			layout.Children.Add(m_translateButton = new Button {
				Text = "Translate",
			});

			layout.Children.Add(m_callButton = new Button {
				Text = "Call",
				IsEnabled = false,
			});
		}

		private void AddListeners()
		{
			m_translateButton.Clicked += translateButton_Clicked;
			m_callButton.Clicked += callButton_Clicked;
		}

		private async void callButton_Clicked (object sender, EventArgs e)
		{
			if(await this.DisplayAlert("Call Verification", "Would you like to call: " + m_callButton.Text.Substring(5) + "?", "Yes", "No"))
			{
				var dialer = DependencyService.Get<IDialer>();
				if(dialer != null)
				{
					dialer.Dial(m_callButton.Text.Substring(5));
				}
			}

		}

		private void alertTask_finished(bool success)
		{
			
		}

		private void translateButton_Clicked (object sender, EventArgs e)
		{
			string number = m_enterNumber.Text;
			string translatedNumber = Core.PhonewordTranslator.ToNumber(number);

			if(String.IsNullOrEmpty(translatedNumber))
			{
				m_callButton.Text = m_callButton.Text.Substring(0, 4);
				m_callButton.IsEnabled = false;
				return; // return early if the number wasn't translated
			}

			m_callButton.Text = m_callButton.Text.Substring(0, 4) + " " + translatedNumber;
			m_callButton.IsEnabled = true;
		}
	}
}

