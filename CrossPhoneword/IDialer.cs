﻿using System;

namespace CrossPhoneword
{
	public interface IDialer
	{
		bool Dial(string number);
	}
}

